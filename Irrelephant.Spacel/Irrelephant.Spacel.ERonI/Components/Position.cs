﻿using Irrelephant.Spacel.ERonI.Core;

namespace Irrelephant.Spacel.ERonI.Components
{
    class Position : Component
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
