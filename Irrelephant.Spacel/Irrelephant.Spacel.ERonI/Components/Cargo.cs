﻿using Irrelephant.Spacel.ERonI.Core;
using System.Collections.Generic;

namespace Irrelephant.Spacel.ERonI.Components
{
    class Cargo : Component
    {
        public Cargo()
        {
            Items = new List<Entity>();
        }

        public int Capacity { get; set; }
        public virtual List<Entity> Items { get; set; }
    }
}
