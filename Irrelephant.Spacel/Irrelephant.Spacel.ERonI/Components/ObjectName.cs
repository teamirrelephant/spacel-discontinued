﻿using Irrelephant.Spacel.ERonI.Core;

namespace Irrelephant.Spacel.ERonI.Components
{
    class ObjectName : Component
    {
        public string Name { get; set; }
    }
}
