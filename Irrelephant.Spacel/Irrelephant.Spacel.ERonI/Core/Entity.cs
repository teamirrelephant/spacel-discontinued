﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Irrelephant.Spacel.ERonI.Core
{
    [Table("Entities")]
    public class Entity
    {
        [Key]
        public int Id { get; private set; }
        public string GUID { get; private set; }
        public virtual List<Component> Components { get; set; }
        [NotMapped]
        public TypeMask Mask { get; private set; }

        private string TypeMaskString
        {
            get{ return Mask.ToString(); }
            set{ Mask = new TypeMask(value); }
        }

        public Entity()
        {
            Components = new List<Component>();
            Mask = new TypeMask();
        }

        public Entity(string GUID) 
            : this()
        {
            this.GUID = GUID;
        }

        public void Add(Component component)
        {
            Components.Add(component);
            component.ParentEntity = this;
            Mask |= component.GetMask();
        }

        public void Delete(TypeMask componentMask)
        {
            if (Contains(componentMask))
            {
                int removeIndex = Components.FindIndex(c => c.GetMask() == componentMask);
                Components.RemoveAt(removeIndex);
                Mask &= ~componentMask;
            }
        }

        public Component Get(TypeMask componentMask)
        {
            return Components.Find(c => c.GetMask() == componentMask);
        }

        public T Get<T>() where T : Component
        {
            return (T) Get(Component.GetMask<T>());
        }

        public bool Contains<T>() where T : Component
        {
            return Contains(Component.GetMask<T>());
        }

        public bool Contains(TypeMask componentMask)
        {
            return (Mask & componentMask) == componentMask;
        }

        public class EntityConfiguration : EntityTypeConfiguration<Entity>
        {
            public EntityConfiguration()
            {
                Property(entity => entity.Id);
                Property(entity => entity.GUID);
                Property(entity => entity.TypeMaskString);
            }
        }
    }
}
