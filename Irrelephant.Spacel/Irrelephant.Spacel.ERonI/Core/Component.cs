﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Irrelephant.Spacel.ERonI.Core
{
    [Table("Components")]
    public abstract class Component
    {
        [Key]
        public int Id { get; private set; }
        [Required]
        public int ParentEntityId { get; private set; }
        [Required]
        public virtual Entity ParentEntity { get; set; }

        private static DbMasksMapper componentsMap;

        static Component()
        {
            componentsMap = new DbMasksMapper(typeof(Component));
        }

        public TypeMask GetMask()
        {
            return componentsMap.GetMask(this.GetType());
        }

        public static TypeMask GetMask<T>() where T : Component
        {
            return componentsMap.GetMask(typeof(T));
        }

        public class ComponentConfiguration : EntityTypeConfiguration<Component>
        {
            public ComponentConfiguration()
            {
                Property(component => component.Id);
                Property(component => component.ParentEntityId);
            }
        }
    }
}
