﻿using System.ComponentModel.DataAnnotations;

namespace Irrelephant.Spacel.ERonI.Core
{
    public class TypeMaskPair
    {
        [Key]
        public int TypeId { get; set; }
        [Required]
        public string TypeName { get; set; }

        public TypeMaskPair()
        {

        }

        public TypeMaskPair(string typeName)
        {
            TypeName = typeName;
        }
    }
}
