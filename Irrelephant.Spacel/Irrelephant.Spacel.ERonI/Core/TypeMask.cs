﻿using System.Collections;

namespace Irrelephant.Spacel.ERonI.Core
{
    public partial class TypeMask
    {
        internal BitArray BitsArray { get; private set; }

        public int Length
        {
            get { return BitsArray.Length; }
        }

        public TypeMask()
        {
            BitsArray = new BitArray(1);
        }

        public TypeMask(int bitsNumber)
        {
            BitsArray = new BitArray(bitsNumber, false);
        }

        public TypeMask(TypeMask image)
        {
            BitsArray = new BitArray(image.BitsArray);
        }

        public TypeMask(string image) 
            : this(image.Length)
        {
            for (int i = 0; i < image.Length; i++)
                Set(i, image[i] == '1' ? true : false);
        }

        public void Set(int position, int value)
        {
            Set(position, value != 0 ? true : false);
        }

        public void Set(int position, bool value)
        {
            BitsArray.Set(position, value);
        }

        public void SetAll(int value)
        {
            SetAll(value != 0 ? true : false);
        }

        public void SetAll(bool value)
        {
            BitsArray.SetAll(value);
        }

        //Shitty implementation. Equals which returns ReferenceEquals != ice
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (object.ReferenceEquals(this, obj))
                return true;

            if (this.GetType() != obj.GetType())
                return false;

            return this.Equals(obj as TypeMask);
        }

        public bool Equals(TypeMask typeMask)
        {
            if (this.Length != typeMask.Length)
                return false;

            for (int i = 0; i < this.Length - 1; i++)
                if (this.BitsArray.Get(i) != typeMask.BitsArray.Get(i))
                    return false;

            return true;
        }

        public static TypeMask ShiftLeft(TypeMask value, int shift)
        {
            TypeMask m = new TypeMask(value.Length);
            if (shift >= value.Length)
                return m;
            for (int i = shift; i < value.Length - 1; i++)
                m.BitsArray.Set(i - shift, value.BitsArray.Get(i));
            return m;
        }

        public static TypeMask ShiftRight(TypeMask value, int shift)
        {
            TypeMask m = new TypeMask(value.Length);
            if (shift >= value.Length)
                return m;
            for (int i = value.Length - shift - 1; i > -1; i--)
                m.BitsArray.Set(i + shift, value.BitsArray.Get(i));
            return m;
        }

        public static TypeMask Or(TypeMask left, TypeMask right)
        {
            TypeMask minMask = left.Length > right.Length ? right : left;
            TypeMask maxMask = left.Length > right.Length ? left : right;

            TypeMask m = new TypeMask(maxMask);
            for (int i = 0; i < minMask.Length; i++)
                m.BitsArray.Set(i, maxMask.BitsArray.Get(i) || minMask.BitsArray.Get(i));
            return m;
        }

        public static TypeMask And(TypeMask left, TypeMask right)
        {
            TypeMask minMask = left.Length > right.Length ? right : left;
            TypeMask maxMask = left.Length > right.Length ? left : right;

            TypeMask m = new TypeMask(maxMask);
            m.SetAll(0);
            for (int i = 0; i < minMask.Length; i++)
                m.BitsArray.Set(i, maxMask.BitsArray.Get(i) && minMask.BitsArray.Get(i));
            return m;
        }

        public static TypeMask Not(TypeMask value)
        {
            TypeMask m = new TypeMask(value);
            m.BitsArray.Not();
            return m;
        }

        public override string ToString()
        {
            char[] array = new char[BitsArray.Length];
            for (int i = 0; i < BitsArray.Length; i++)
                array[i] = BitsArray.Get(i) != false ? '1' : '0';
            return new string(array);
        }
    }

    //Probably should be in another file (partial keyword)
    public partial class TypeMask
    {
        public static bool operator ==(TypeMask left, TypeMask right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(TypeMask left, TypeMask right)
        {
            return !left.Equals(right);
        }

        public static TypeMask operator <<(TypeMask value, int shift)
        {
            return ShiftLeft(value, shift);
        }

        public static TypeMask operator >>(TypeMask value, int shift)
        {
            return ShiftRight(value, shift);
        }

        public static TypeMask operator |(TypeMask left, TypeMask right)
        {
            return Or(left, right);
        }

        public static TypeMask operator &(TypeMask left, TypeMask right)
        {
            return And(left, right);
        }

        public static TypeMask operator ~(TypeMask value)
        {
            return Not(value);
        }
    }
}
