﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Irrelephant.Spacel.ERonI.Core
{
    public class DbMasksMapper
    {
        private Dictionary<Type, TypeMask> map;

        public DbMasksMapper(Type baseType)
        {
            map = new Dictionary<Type, TypeMask>();
            InitializeMap(baseType);
        }

        public TypeMask GetMask(Type type)
        {
            return map[type];
        }

        private void InitializeMap(Type baseType)
        {
            List<Type> uncodedTypesList = LoadInheritedTypesList(baseType);
            SynchronizeTypesWithDatabase(uncodedTypesList);
            AssignCodesWithInheritedType(uncodedTypesList);
        }

        private void AssignCodesWithInheritedType(List<Type> uncodedTypesList)
        {
            List<TypeMaskPair> codedTypesList = LoadKnownTypesPairs();
            AssignMasks(uncodedTypesList, codedTypesList);
        }

        private void AssignMasks(List<Type> uncodedTypesList, List<TypeMaskPair> codedTypesList)
        {
            TypeMask initialMask = new TypeMask(uncodedTypesList.Count);
            initialMask.Set(0, 1);
            foreach (Type type in uncodedTypesList)
            {
                TypeMaskPair codedPair = codedTypesList.Find(pair => pair.TypeName == type.Name);
                map.Add(type, initialMask >> (int)codedPair.TypeId - 1);
            }
        }

        private void SynchronizeTypesWithDatabase(List<Type> uncodedTypesList)
        {
            List<TypeMaskPair> codedTypesList = LoadKnownTypesPairs();
            List<Type> remainigUncodedTypesList = FindRemainingTypes(uncodedTypesList, codedTypesList);
            PushRemainingTypesInDatabase(remainigUncodedTypesList);
        }

        private void PushRemainingTypesInDatabase(List<Type> remainigUncodedTypesList)
        {
            using(var db = new EntitiesContext())
            {
                foreach (Type type in remainigUncodedTypesList)
                    db.MappedTypes.Add(new TypeMaskPair(type.Name));
                db.SaveChanges();
            }
        }

        private List<Type> FindRemainingTypes(List<Type> uncodedTypesList, List<TypeMaskPair> codedTypesList)
        {
            List<Type> remainingUncodedTypesList = new List<Type>();
            foreach (Type type in uncodedTypesList)
            {
                TypeMaskPair codedPair = codedTypesList.Find(pair => pair.TypeName == type.Name);
                if (codedPair == default(TypeMaskPair))
                    remainingUncodedTypesList.Add(type);
            }
            return remainingUncodedTypesList;
        }

        private List<TypeMaskPair> LoadKnownTypesPairs()
        {
            using (var db = new EntitiesContext())
            {
                var knownTypes = from types in db.MappedTypes
                                 select types;

                if (knownTypes.Count() != 0)
                    return new List<TypeMaskPair>(knownTypes);
                else
                    return new List<TypeMaskPair>();
            }
        }

        private List<Type> LoadInheritedTypesList(Type baseType)
        {
            IEnumerable<Type> inherited = Assembly.GetEntryAssembly().GetTypes().ToList()
                                                .Where(t => t.IsSubclassOf(baseType));
            return new List<Type>(inherited);
        }
    }
}
