namespace Irrelephant.Spacel.ERonI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Entities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GUID = c.String(),
                        TypeMaskString = c.String(),
                        Cargo_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Components", t => t.Cargo_Id)
                .Index(t => t.Cargo_Id);
            
            CreateTable(
                "dbo.Components",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentEntityId = c.Int(nullable: false),
                        Capacity = c.Int(),
                        Name = c.String(),
                        X = c.Int(),
                        Y = c.Int(),
                        Power = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Entities", t => t.ParentEntityId, cascadeDelete: true)
                .Index(t => t.ParentEntityId);
            
            CreateTable(
                "dbo.TypeMaskPairs",
                c => new
                    {
                        TypeId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Entities", "Cargo_Id", "dbo.Components");
            DropForeignKey("dbo.Components", "ParentEntityId", "dbo.Entities");
            DropIndex("dbo.Components", new[] { "ParentEntityId" });
            DropIndex("dbo.Entities", new[] { "Cargo_Id" });
            DropTable("dbo.TypeMaskPairs");
            DropTable("dbo.Components");
            DropTable("dbo.Entities");
        }
    }
}
