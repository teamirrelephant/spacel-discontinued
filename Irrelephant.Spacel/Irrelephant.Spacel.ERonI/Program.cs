﻿using Irrelephant.Spacel.ERonI.Actions;
using Irrelephant.Spacel.ERonI.Components;
using Irrelephant.Spacel.ERonI.Core;
using System;
using System.Collections.Generic;


/*Examples of working with this shit
*/

namespace Irrelephant.Spacel.ERonI
{
    class Program
    {
        private static EntitiesDatabase edb = new EntitiesDatabase(); //for getting and setting entities

        public static void Main(string[] args)
        {
            SeedSomeEntities();
            ReadEntities();
            DoSmth();
        }

        private static void DoSmth()
        {
            foreach (var entity in edb.GetAllEntities())
            {
                MoveActions.MoveTo(entity, new Position { X = 100, Y = 200 });
            }
        }

        private static void ReadEntities()
        {
            foreach (var entity in edb.GetEntities(e => e.Contains<Cargo>()))
            {
                //for multiple objects write e.Contains(Component.GetMas<Cargo>() | Component.GetMask<ObjectName>())

                ObjectName name = entity.Get<ObjectName>();
                Cargo cargo = entity.Get<Cargo>();

                Console.WriteLine(entity.GUID);
                Console.WriteLine(name.Name);
                Console.WriteLine(cargo.Capacity);
            }
            Console.Read();
        }

        private static void SeedSomeEntities()
        {
            List<Entity> defaultEntities = new List<Entity>();
            Random rand = new Random();

            CreateItems_EntitiesWithoutGUID(defaultEntities, rand);
            CreateShips_EntitiesWithGUID(defaultEntities, rand);
            CreateComplexEntitiy_ShipWithCargoForExample(defaultEntities, rand);

            edb.AddEntities(defaultEntities);
        }

        private static void CreateComplexEntitiy_ShipWithCargoForExample(List<Entity> defaultEntities, Random rand)
        {
            for (int i = 0; i < 1; i++)
            {
                Entity entity = new Entity(Guid.NewGuid().ToString());
                entity.Add(new Position { X = rand.Next(), Y = rand.Next() }); //use objectInitializer or provide default constructor and write yours wich you need
                entity.Add(new ObjectName { Name = "Entity" + rand.Next(0, 1000) });
                entity.Add(new ShipCore { Power = rand.Next() });

                Cargo cargo = new Cargo { Capacity = rand.Next() };
                cargo.Items.AddRange(defaultEntities.GetRange(0, 4));

                entity.Add(cargo);

                defaultEntities.Add(entity);
            }
        }

        private static void CreateShips_EntitiesWithGUID(List<Entity> defaultEntities, Random rand)
        {
            for (int i = 0; i < 10; i++)
            {
                Entity entity = new Entity(Guid.NewGuid().ToString());
                entity.Add(new Position { X = rand.Next(), Y = rand.Next() });
                entity.Add(new ObjectName { Name = "Entity" + rand.Next(0, 1000) });
                entity.Add(new ShipCore { Power = rand.Next() });

                defaultEntities.Add(entity);
            }
        }

        private static void CreateItems_EntitiesWithoutGUID(List<Entity> defaultEntities, Random rand)
        {
            for (int i = 0; i < 3; i++)
            {
                Entity entity = new Entity();
                entity.Add(new Position { X = rand.Next(), Y = rand.Next() });
                entity.Add(new ObjectName { Name = "Entity" + rand.Next(0, 1000) });
                entity.Add(new ShipCore { Power = rand.Next() });

                defaultEntities.Add(entity);
            }
        }
    }
}
