﻿using Irrelephant.Spacel.ERonI.Core;
using System.Data.Entity;

namespace Irrelephant.Spacel.ERonI
{
    public class EntitiesContext : DbContext
    {
        public DbSet<TypeMaskPair> MappedTypes { get; set; }
        public DbSet<Entity> Entities { get; set; }

        //Do you really need this one?
        public EntitiesContext()
            : base("EntitiesDb")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entity>()
                .HasMany(entity => entity.Components)
                .WithRequired(component => component.ParentEntity)
                .HasForeignKey(component => component.ParentEntityId)
                .WillCascadeOnDelete();

            modelBuilder.Configurations.Add(new Entity.EntityConfiguration());
            modelBuilder.Configurations.Add(new Component.ComponentConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
