﻿using Irrelephant.Spacel.ERonI.Core;

namespace Irrelephant.Spacel.ERonI.Actions
{
    //This one smells like "COMMAND" design pattern (http://en.wikipedia.org/wiki/Command_pattern) - make it conform to it.
    static class AttackActions
    {
        public static void Attack(Entity attacker, Entity target)
        {

        }

        public static void Attack(Entity attacker, params Entity[] targets)
        {

        }
    }
}
