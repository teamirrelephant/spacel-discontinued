﻿using Irrelephant.Spacel.ERonI.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Irrelephant.Spacel.ERonI
{
    class EntitiesDatabase
    {
	//This should be a singletone
        private EntitiesContext db = new EntitiesContext();

        //There is already a Find method in the dbset
        public Entity GetEntity(string GUID)
        {
            return db.Entities.First(entity => entity.GUID == GUID);
        }

        //Why use lists when you just need to get READ access to it
        //Use Enumerators instead
        public List<Entity> GetEntities(Func<Entity, bool> selector)
        {
            return db.Entities.Where(selector).ToList();
        }

        //db.Entities.AsEumerable();
        public List<Entity> GetAllEntities()
        {
            return (from entity in db.Entities
                    select entity).ToList();
        }

        public void AddEntity(Entity entity)
        {
            db.Entities.Add(entity);
            db.SaveChanges();
        }

        //I betcha user can add the entire collection by himself
        public void AddEntities(List<Entity> entitiesList)
        {
            foreach (var entity in entitiesList)
                db.Entities.Add(entity);
            db.SaveChanges();
        }

        public void DeleteEntity(string GUID)
        {
            Entity entityToDel = GetEntity(GUID);
            db.Entities.Remove(entityToDel);
            db.SaveChanges();
        }

        //Why do you need this when you already call save on all the other methods??
        public void SaveChanges()
        {
            db.SaveChanges();
        }
    }
}
