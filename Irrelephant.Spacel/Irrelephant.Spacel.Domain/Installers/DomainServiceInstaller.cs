﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Irrelephant.Spacel.Domain.Services.Interfaces;

namespace Irrelephant.Spacel.Domain.Installers
{
    public class DomainServiceInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                .BasedOn(typeof(IDomainService<>))
                .LifestylePerWebRequest()
                .WithServiceDefaultInterfaces());
        }
    }
}
