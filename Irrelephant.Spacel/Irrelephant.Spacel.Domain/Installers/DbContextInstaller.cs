﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Irrelephant.Spacel.Domain.EntityFramework;

namespace Irrelephant.Spacel.Domain.Installers
{
    public class DbContextInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                    .BasedOn<ISpacelContext>()
                    .LifestylePerWebRequest()
                    .WithServiceDefaultInterfaces());
        }
    }
}
