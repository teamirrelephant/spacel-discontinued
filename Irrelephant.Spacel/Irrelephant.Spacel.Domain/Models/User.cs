﻿using Irrelephant.Spacel.Domain.Models.Interfaces;

namespace Irrelephant.Spacel.Domain.Models
{
    public class User : IBaseDomainModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
    }
}
