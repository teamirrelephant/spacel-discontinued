﻿using System.Linq;
using Irrelephant.Spacel.Domain.EntityFramework;
using Irrelephant.Spacel.Domain.Models;
using Irrelephant.Spacel.Domain.Services.Interfaces;

namespace Irrelephant.Spacel.Domain.Services
{
    public class UserDomainService : DomainService<User>, IUserDomainService
    {
        public UserDomainService(ISpacelContext spacelContext) : base(spacelContext)
        {
        }

        public User GetByUserName(string userName)
        {
            return Context.Users.FirstOrDefault(x => x.UserName == userName);
        }
    }
}
