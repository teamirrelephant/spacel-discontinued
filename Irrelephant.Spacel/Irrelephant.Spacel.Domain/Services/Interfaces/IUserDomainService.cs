﻿using Irrelephant.Spacel.Domain.Models;

namespace Irrelephant.Spacel.Domain.Services.Interfaces
{
    public interface IUserDomainService : IDomainService<User>
    {
        User GetByUserName(string userName);
    }
}
