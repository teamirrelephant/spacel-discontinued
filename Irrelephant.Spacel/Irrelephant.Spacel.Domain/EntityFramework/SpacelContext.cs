﻿using System.Data.Entity;
using System.Reflection;
using Irrelephant.Spacel.Domain.EntityFramework.Configurations;
using Irrelephant.Spacel.Domain.Models;

namespace Irrelephant.Spacel.Domain.EntityFramework
{
    public class SpacelContext : DbContext, ISpacelContext
    {
        public DbSet<User> Users { get; set; }

        public SpacelContext(): base("SpacelContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(UserConfiguration)));
            base.OnModelCreating(modelBuilder);
        }
    }
}
