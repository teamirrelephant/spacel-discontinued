﻿using System.Data.Entity.ModelConfiguration;
using Irrelephant.Spacel.Domain.Models;

namespace Irrelephant.Spacel.Domain.EntityFramework.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(x => x.Id);
        }
    }
}
