﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Irrelephant.Spacel.Configuration.WindsorCastle.Installers
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromAssemblyNamed("Irrelephant.Spacel.Web")
                .BasedOn<Controller>()
                .LifestyleTransient());
        }
    }
}