﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Irrelephant.Spacel.Configuration.WindsorCastle.Installers;
using Irrelephant.Spacel.Configuration.WindsorCastle.Plumbing;
using Irrelephant.Spacel.Domain.Installers;
using Irrelephant.Spacel.Infrastructure.WindsorCastle;
using Irrelephant.Spacel.Presentation.Installers;

namespace Irrelephant.Spacel.Configuration.WindsorCastle
{
    public class WindsorCastleConfig
    {
        public static void RegisterInstallers()
        {
            var container = IoC.ContainerInstance;
            container.Register(Component.For<IWindsorContainer>().Instance(container));

            container.Install(
                new ControllerInstaller(),
                new PresentationServiceInstaller(),
                new DomainServiceInstaller(),
                new DbContextInstaller());

            container.Register(Component.For<IControllerFactory>().ImplementedBy<WindsorControllerFactory>().LifeStyle.Singleton);
            var controllerFactory = container.Resolve<IControllerFactory>();
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }
    }
}