﻿using Microsoft.AspNet.Identity;

namespace Irrelephant.Spacel.Presentation.Identity
{
    public class ApplicationUser : IUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
    }
}
