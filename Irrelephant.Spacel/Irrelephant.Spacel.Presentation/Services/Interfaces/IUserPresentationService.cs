﻿namespace Irrelephant.Spacel.Presentation.Services.Interfaces
{
    public interface IUserPresentationService : IPresentationService
    {
        string GetFirstUserName();
        bool AddToDB(string text);
    }
}
