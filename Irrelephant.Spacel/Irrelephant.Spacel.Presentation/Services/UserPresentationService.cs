﻿using System.Linq;
using Irrelephant.Spacel.Domain.Models;
using Irrelephant.Spacel.Domain.Services.Interfaces;
using Irrelephant.Spacel.Presentation.Services.Interfaces;

namespace Irrelephant.Spacel.Presentation.Services
{
    public class UserPresentationService : IUserPresentationService
    {
        private readonly IUserDomainService _userDomainService;

        public UserPresentationService(IUserDomainService userDomainService)
        {
            _userDomainService = userDomainService;
        }

        public string GetFirstUserName()
        {
            var user = _userDomainService.GetAll().FirstOrDefault();
            if (user == null)
            {
                return "empty";
            }

            return user.UserName;
        }

        public bool AddToDB(string text)
        {
            var user = _userDomainService.Create(new User() {UserName = text});

            if (user == null)
            {
                return true;
            }

            return false;
        }
    }
}
