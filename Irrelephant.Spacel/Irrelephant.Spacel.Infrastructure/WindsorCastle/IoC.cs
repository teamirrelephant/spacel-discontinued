﻿using Castle.Windsor;

namespace Irrelephant.Spacel.Infrastructure.WindsorCastle
{
    public class IoC
    {
        private static readonly IWindsorContainer Container = new WindsorContainer();

        public static IWindsorContainer ContainerInstance
        {
            get
            {
                return Container;
            }
        }
    }
}
