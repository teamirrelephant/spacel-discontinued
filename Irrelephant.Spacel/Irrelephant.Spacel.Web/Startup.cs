﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Irrelephant.Spacel.Web.Startup))]
namespace Irrelephant.Spacel.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
