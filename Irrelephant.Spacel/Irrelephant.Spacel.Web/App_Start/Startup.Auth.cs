﻿using System;
using Irrelephant.Spacel.Presentation.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Irrelephant.Spacel.Web
{
	public partial class Startup
	{
        public static Func<UserManager<ApplicationUser>> UserManagerFactory { get; set; }

        public Startup()
        {
            UserManagerFactory = () =>
            {
                var userManager = new UserManager<ApplicationUser>(new CustomUserStore());
                return userManager;
            };
        }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                LogoutPath = new PathString("/Account/LogOff"),
                ExpireTimeSpan = TimeSpan.FromDays(7)
            });

            app.SetDefaultSignInAsAuthenticationType(DefaultAuthenticationTypes.ApplicationCookie);
        }
	}
}