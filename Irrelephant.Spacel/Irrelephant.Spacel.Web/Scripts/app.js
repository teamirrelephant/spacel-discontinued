﻿var spacel = angular.module('spacel', []);

spacel.controller('MainUIController', function ($scope) {
    $scope.currentRoom = "Cargo Hold";
    $scope.rooms = [
        "Bridge",
        "Engineering",
        "Cargo Hold",
        "Armory",
        "Weapon Control",
        "Lab",
        "Canteen"
    ];

    $scope.localUsers = [
        new User("Usr1", "Rookie II", "lol1@lol.lol"),
        new User("Usr3", "Rookie II", "lol2@lol.lol"),
        new User("Usr4", "Rookie II", "lol3@lol.lol"),
        new User("Usr5", "Rookie II", "lol4@lol.lol")
    ];

    $scope.currentUser = new User("Pilot", "Rookie", "email");

    $.ajax({
        url: serviceUrls.getPlayerInventory,
        method: 'GET',
        success: function (data) {
            $scope.currentInventory = data;
            $scope.currentShip = {
                inventory: data,
                user: $scope.currentUser,
                weaponFactor: 1.0,
                healthFactor: 1.0
        }
        }
    });

    $scope.gameDate = {
        galacticDate: new Date().toDateString()
    }

    $scope.transferTo = function (room) {
        if ($scope.rooms.indexOf(room) != -1) {
            $scope.currentRoom = room;
        }
    }
});