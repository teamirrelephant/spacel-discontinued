﻿var User = function (username, rank, email) {
    this.username = username;
    this.rank = rank;
    this.email = email;
    this.hp = 666;
    this.maxUserHp = 666;

    this.toString = function () {
        return this.rank + " " + this.username;
    }

    this.getProfileImage = function () {
        return "http://placehold.it/230x230";
    }

    this.getSmallProfileImage = function() {
        return "http://placehold.it/40x40";
    }

    this.getUserHp = function(factor)
    {
        return this.hp > 0 ? ("HP: " + this.hp + " / " + this.maxUserHp) : "DEAD";
    }

    this.damage = function(value) {
        this.hp -= value;
        if (this.hp <= 0) {
            console.log("Pizdidal");
        }
    }
}