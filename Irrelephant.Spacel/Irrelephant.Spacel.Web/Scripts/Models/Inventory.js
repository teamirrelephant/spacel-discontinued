﻿var Inventory = function () {
    this.items = [];

    this.addItem = function(itemToAdd) {
        stack = _.find(this.items, function(item) {
            return item.itemName === itemToAdd.itemName;
        });

        if (stack != undefined) {
            stack.combine(itemToAdd);
        } else {
            this.items.push(itemToAdd);
        }
    };

};