﻿var ItemStack = function (stackId, itemName, quantity) {
    this.stackId = stackId;
    this.itemName = itemName;
    this.quantity = quantity;

    this.combine = function (stack1, stack2) {
        if (stack1.itemName != stack1.itemName) throw "Trying to combine stacks of different items!";
        else return new ItemStack(stack1.stackId, stack1.itemName, stack1.quantity + stack1.quantity);
    }
}
