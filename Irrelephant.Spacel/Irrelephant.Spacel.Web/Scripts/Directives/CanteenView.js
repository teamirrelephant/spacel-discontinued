﻿var canteenView = angular.module('spacel')
    .directive('canteenView', function() {
        return {
            restrict: 'E',
            templateUrl: '/Directives/CanteenView',
            scope: {
                ship: '='
            }
        }
    });