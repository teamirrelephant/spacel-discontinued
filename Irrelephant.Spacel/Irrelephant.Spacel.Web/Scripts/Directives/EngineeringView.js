﻿var engineeringView = angular.module('spacel')
    .directive('engineeringView', function() {
        return {
            restrict: 'E',
            templateUrl: '/Directives/EngineeringView',
            scope: {
                ship: '=',
                user: '='
            },
            link: function(scope, element, attr) {
                scope.update = function() {
                    scope.hp = scope.user.getUserHp(scope.ship.healthFactor);
                }
                scope.update();
            }
        }
    });