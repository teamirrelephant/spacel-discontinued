﻿var labView = angular.module('spacel')
    .directive('labView', function() {
        return {
            restrict: 'E',
            templateUrl: '/Directives/LabView',
            scope: {
                ship: '='
            },
            link: function (scope, element, attributes) {
                scope.subtractLumias = function(amount) {
                    var amountLeft = amount;
                    var lumias = _.filter(scope.ship.inventory, function (item) { return item.name == "Lumia 920"; });
                    console.log(lumias);
                    for (var i = 0; i < lumias.length; i++) {
                        console.log("i=" + i +"left="+amountLeft);
                        if (parseInt(lumias[i].quantity) <= amountLeft) {
                            console.log("1: " + lumias[i].quantity);
                            amountLeft -= parseInt(lumias[i].quantity);
                            lumias = _.without(lumias, lumias[i]);
                            scope.ship.inventory = _.without(scope.ship.inventory, lumias[i]);
                            i--;
                        } else {
                            console.log("2: " + lumias[i].quantity);
                            lumias[i].quantity -= amountLeft;
                            amountLeft = 0;
                        }

                        if (amountLeft == 0) {
                            scope.updateLumiasCount();
                            return;
                        }
                    };
                }

                scope.upgradeShip = function (target) {
                    scope.subtractLumias(30);
                    switch(target) {
                        case "Weapons": scope.ship.weaponFactor *= 1.3;
                            console.log(scope.ship.weaponFactor);
                        break;
                    case "Hull":
                        scope.ship.healthFactor *= 1.3;
                        console.log(scope.ship.healthFactor);
                        break;
                    }
                }

                scope.updateLumiasCount = function() {
                    var lumias = _.filter(scope.ship.inventory, function (item) { return item.name == "Lumia 920"; });
                    scope.lumiasCount = _.reduce(lumias, function (count, item) {
                        return count + parseInt(item.quantity);
                    }, 0);
                }

                scope.isAbleToUpgrade = function() {
                    return scope.lumiasCount >= 30;
                }

                scope.availableUpgrades = ["Weapons", "Hull"];
                scope.updateLumiasCount();
            }
        }
    });