﻿var bridgeView = angular.module('spacel')
    .directive('bridgeView', function() {
        return {
        restrict: 'E',
        templateUrl: '/Directives/BridgeView',
        scope: {
            location: '=',
            users: '=',
            ship: '='
        },
        link: function (scope, element, attributes) {
            scope.availableActions = ["Attack", "Trade", "Hail"];
            scope.availableWeapons = ["Railgun", "Glass Cannon"];

            scope.targetThis = function (user) {
                console.log("target locked!" + user.username);
                scope.target = user;
            }
            scope.executeAction = function(action, weapon) {
                console.log("Action executed @ " + scope.target.username + " Action = " + action + " Weapon = " + weapon);
                if (action == "Attack") {
                    scope.target.damage(weapon == "Railgun" ? 10 * scope.ship.weaponFactor : 30 * scope.ship.weaponFactor);
                }
            }
        }
    }
});