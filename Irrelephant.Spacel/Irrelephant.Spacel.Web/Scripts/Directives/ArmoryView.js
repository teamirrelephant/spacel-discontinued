﻿var armoryView = angular.module('spacel')
    .directive('armoryView', function() {
        return {
            restrict: 'E',
            templateUrl: '/Directives/ArmoryView',
            scope: {
                ship: '='
            }
        }
    });