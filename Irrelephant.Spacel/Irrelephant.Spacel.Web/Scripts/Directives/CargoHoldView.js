﻿var cargoHoldView = angular.module('spacel')
    .directive('cargoHoldView', function () {
        return {
            restrict: 'E',
            templateUrl: '/Directives/CargoHoldView',
            scope: {
                inventory: '='
            },
            link: function (scope, element, attrubutes) {
                $('#cargo-hold-items-wrapper').niceScroll().resize();
            }
        }
    });