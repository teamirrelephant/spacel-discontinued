﻿var weaponControlView = angular.module('spacel')
    .directive('weaponControlView', function() {
        return {
            restrict: 'E',
            templateUrl: '/Directives/WeaponControlView',
            scope: {
                ship: '='
            }
        }
    });