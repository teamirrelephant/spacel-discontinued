﻿using System.Web.Mvc;
using System.Web.Routing;
using Irrelephant.Spacel.Configuration.WindsorCastle;

namespace Irrelephant.Spacel.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            WindsorCastleConfig.RegisterInstallers();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
