﻿using System.Web.Mvc;
using Irrelephant.Spacel.Presentation.Services.Interfaces;

namespace Irrelephant.Spacel.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserPresentationService _userPresentationService;

        public HomeController(IUserPresentationService userPresentationService)
        {
            _userPresentationService = userPresentationService;
        }

        public ViewResult Index()
        {
            ViewBag.UserName = User.Identity.Name ?? "Anonymous Pilot";
            return View();
        }
    }
}