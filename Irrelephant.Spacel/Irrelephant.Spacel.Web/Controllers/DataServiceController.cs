﻿using System.Web.Mvc;

namespace Irrelephant.Spacel.Web.Controllers
{
    public class DataServiceController : Controller
    {
        public JsonResult GetPlayerInventory()
        {
            return Json(new object[] {
         new { name = "Veldspar", quantity = "15k" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "Lumia 920", quantity = "40" },
         new { name = "Lumia 920", quantity = "40" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "Lumia 920", quantity = "40" },
         new { name = "Lumia 920", quantity = "40" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "Veldspar", quantity = "15k" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Railgun", quantity = "1" },
         new { name = "150mm Lead Slugs", quantity = "300" }
         }, JsonRequestBehavior.AllowGet);
        }
    }
}