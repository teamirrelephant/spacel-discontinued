﻿using System.Web.Mvc;

namespace Irrelephant.Spacel.Web.Controllers
{
    public class DirectivesController : Controller
    {
        public ActionResult CargoHoldView()
        {
            return View();
        }

        public ActionResult ArmoryView()
        {
            return View();
        }

        public ActionResult EngineeringView()
        {
            return View();
        }

        public ActionResult BridgeView()
        {
            return View();
        }

        public ActionResult LabView()
        {
            return View();
        }

        public ActionResult WeaponControlView()
        {
            return View();
        }

        public ActionResult CanteenView()
        {
            return View();
        }
    }
}