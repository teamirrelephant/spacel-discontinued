# README #

# Enviroment Setup #
Check out [detailed guide](https://bitbucket.org/teamirrelephant/spacel/wiki/Environment%20Setup) on how to setup your environment for the first use.

# Getting Started with Ron2 #
Here is a [detailed guide](https://bitbucket.org/teamirrelephant/spacel/wiki/Using%20Ron2) on how to use QnA's component-based model.